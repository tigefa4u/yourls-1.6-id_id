#YOURLS 1.6 Indonesia TRANSLATIONS

Indonesia translations for YOURLS 1.6.

follow [@tigefa_team](http://twitter.com/tigefa_team)

http://tigefa4u.github.io

##Install YOURLS language

If translation files for your language are available:

1. In `config.php` add or edit the following line: `define( 'YOURLS_LANG', 'id_ID' );`
2. In directory user/languages, drop the two files `id_ID.po` et `id_ID.mo`

Of course, replace `id_ID` with the appropriate language code.

## What are the available languages already?

See http://yourls.org/translations

#### License
Do whatever the hell you want with it.

[![githalytics.com alpha](https://cruel-carlota.pagodabox.com/51e281a952f55ec59467afad65496246 "githalytics.com")](http://githalytics.com/YOURLS/YOURLS.pot)
